class Student {
	constructor(name,email,grades) {
		this.name = name;
		this.email = email;
		this.average = undefined;
		this.isPassed = undefined;
		this.isPassedWithHonors = undefined;

		if(grades.some(grade => typeof grade !== 'number')){
			this.grades = undefined
		} else{
			this.grades = grades;
		}
	}

	login(){

        console.log(`${this.email} has logged in`);
        return this;
    }

    logout(){
        console.log(`${this.email} has logged out`);
        return this;
    }

    listGrades(){
        this.grades.forEach(grade => {
            console.log(grade);
        })
        return this;
    }

    computeAve(){
        let sum = this.grades.reduce((accumulator,num) => accumulator += num)
        this.average = Math.round(sum/4);
        return this;
    }

    willPass() {
        this.isPassed = Math.round(this.computeAve().average) >= 85 ? true : false;
        return this;
    }

    willPassWithHonors() {

       this.isPassedWithHonors = this.willPass().isPassed && Math.round(this.computeAve().average) >= 90 ? true : this.willPass().isPassed ? false : undefined;
       return this;
    }
}

//class Section will allow us to group out students as a section

class Section {
    //every instance of Section class will be instantiated with an empty array for our students.
    constructor(name){
        this.name = name;
        this.students = [];
        this.numberOfStudents = undefined;
        this.passedStudents = undefined;
        this.sectionAve = undefined;

        //add a counter or number of honor students:
        this.honorStudents = undefined;
    }

    //addStudent() will allow us to add instances of the student class as items for our students property.

    addStudent(name,email,grades){

        //A student instace/object will be instantiated with the name, email, grades and pushed into our students property.

        this.students.push(new Student(name,email,grades));

        return this;

    }

    //countHonorStudents will loop over each Student instance in our students array and count the number of students

    countHonorStudents(){
        //accumulate the number of honor students
        let honorCount = 0;
        //loop over each Student instance in the students array
        this.students.forEach(student => {

            //log the student instance currently being iterated.
            //console.log(student);

            //log each student's isPassedWithHonors property:
            //console.log(student.willPassWithHonors().isPassedWithHonors)

            //invoke will pass with honors so we can determine and add whether the student passed with honors in its property
            // student.willPassWithHonors();

            //isPassedWithHonors property of the student should be populated
            // console.log(student);

            //Check if student is passedWithHonors
            //add 1 to a temporary varaible to hold the number of honorStudents
            
            if(student.willPassWithHonors().isPassedWithHonors){
                honorCount++
            };
        })

        //update the honorStudents property with the updated value of count:
        this.honorStudents = honorCount;
        return this;
    }

    //FUNCTION CODING

    getNumberOfStudents(){
        let studentCount = 0;

        this.students.forEach(student => {
            studentCount++
        })

        this.numberOfStudents = studentCount;
        return this;
    }

    countPassedStudents(){
        let passedStudentsCount = 0;

        this.students.forEach(student => {
            if(student.willPass().isPassed){
                passedStudentsCount++
            }
        })
        this.passedStudents = passedStudentsCount;
        return this;

    }

 
    computeSectionAve(){
       let count = 0;

       this.students.forEach(student => {
        let ave = student.computeAve().average
        let total = ave + count 
        count = total
        console.log(total)
       })

       
       this.sectionAve = Math.round(count/5);
       return this;
       
    }
}

let section1A = new Section("Section1A");
//console.log(section1A);

section1A.addStudent("Joy","joy@mail.com",[89,91,92,88]);
section1A.addStudent("Jeff","jeff@mail.com",[81,80,82,78]);
section1A.addStudent("John","john@mail.com",[91,90,92,96]);
section1A.addStudent("Jack","jack@mail.com",[95,92,92,93]);
//console.log(section1A);

//check the details of John from section1A?
    //console.log(section1A.students[2])

//check the average of John from section1A
    //console.log("John's average:",section1A.students[2].computeAve().average);

//check if Jeff from section1A passed?
    //console.log("Did Jeff pass?",section1A.students[1].willPass().isPassed);

//display the number of honor students in our section.
    //section1A.countHonorStudents();

//check the number of students
    console.log("Honor Students:",section1A.countHonorStudents().honorStudents);

//Function Coding
section1A.addStudent("Alex","alex@mail.com",[84,85,85,86])
console.log("Number of Students:", section1A.getNumberOfStudents().numberOfStudents)
console.log("Number of passedStudents:", section1A.countPassedStudents().passedStudents)
console.log("Section Average:", section1A.computeSectionAve().sectionAve)